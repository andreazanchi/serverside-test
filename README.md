#### Dependencies:
* Java 8

#### Tools:
* Gradle

#### Framework:
- Lombok to avoid boilerplate
- Guice to do some basic dependency injection
- Mockito
- Wiremock
- Junit


#### How to build and run tests:
./gradlew clean build

#### How to run:
./gradlew run

#### Behaviour:
The application starts scraping from the berries product list which is hardcoded.
It then extracts the data for each product and follow the links to get additional data such as description and calories.
It aborts immediately if the product list is not reachable.
If any product page is not reachable, it logs it and skips it (still producing the output).
It produces a prettified json.
Without any appender specified, it logs in the console.
