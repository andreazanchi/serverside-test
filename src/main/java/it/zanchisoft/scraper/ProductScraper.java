package it.zanchisoft.scraper;

public class ProductScraper {

    private static final String KCAL_SELECTOR = ".nutritionTable .tableRow0 .nutritionLevel1";
    private static final String PRODUCT_DESCRIPTION_SELECTOR = ".productText p";
    private static final String KCAL_LABEL = "kcal";

    public ProductDetail productDetailFromScraping(DomNavigator domNavigator) {
        String kcal = extractKcal(domNavigator);
        String description = extractDescription(domNavigator);
        return new ProductDetail(kcal, description);
    }

    private String extractDescription(DomNavigator domNavigator) {
        return domNavigator.select(PRODUCT_DESCRIPTION_SELECTOR).stream().findFirst()
                .map(DomElement::text).orElse("");
    }

    private String extractKcal(DomNavigator domNavigator) {
        return domNavigator.select(KCAL_SELECTOR).stream().findFirst()
                .map(DomElement::text).map(this::removeKCalLabel).orElse(null);
    }

    private String removeKCalLabel(String kcal) {
        return kcal.replace(KCAL_LABEL, "");
    }
}
