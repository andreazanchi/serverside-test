package it.zanchisoft.scraper;

public interface DomNavigatorFactory {
    DomNavigator buildFor(String body);
}
