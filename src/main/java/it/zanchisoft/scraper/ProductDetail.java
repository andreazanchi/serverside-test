package it.zanchisoft.scraper;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class ProductDetail {

    private String kCalPer100g;
    private String description;
}
