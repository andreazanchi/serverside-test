package it.zanchisoft.scraper;

import java.util.List;

public interface DomNavigator {
    List<DomElement> select(String cssSelector);
}
