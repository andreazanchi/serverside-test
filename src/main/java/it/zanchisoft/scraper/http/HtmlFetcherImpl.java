package it.zanchisoft.scraper.http;

import it.zanchisoft.scraper.HtmlFetcher;
import lombok.extern.java.Log;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Optional;
import java.util.logging.Level;

@Log
public class HtmlFetcherImpl implements HtmlFetcher {

    public static final String CANNOT_DOWNLOAD = "Can't download %s";

    @Override
    public Optional<String> getBody(String url) {
        Optional<String> body = Optional.empty();
        HttpGet request = new HttpGet(url);
        try (CloseableHttpClient httpClient = HttpClients.createDefault() ){
            HttpResponse httpResponse = httpClient.execute(request);
            if (isValid(httpResponse)) {
                HttpEntity entity = httpResponse.getEntity();
                body = Optional.of(EntityUtils.toString(entity));
            }
        } catch (IOException e) {
            log.severe(String.format(CANNOT_DOWNLOAD, url));
        }
        return body;
    }

    private boolean isValid(HttpResponse httpResponse) {
        return httpResponse.getStatusLine().getStatusCode() == 200;
    }

    @Override
    public Optional<String> buildFullPath(String baseUrl, String relativeUrl) {
        Optional<String> ret = Optional.empty();
        try {
            ret = Optional.of(new URL(new URL(baseUrl), relativeUrl).toString());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return ret;
    }
}
