package it.zanchisoft.scraper;


import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class ProductListScraper {

    private static final String PRODUCT_CONTAINER_SELECTOR = ".product";
    private static final String PRODUCT_NAME_SELECTOR = ".productNameAndPromotions a";
    private static final String PRICE_PER_UNIT_SELECTOR = "div.pricing > p.pricePerUnit";
    private static final String PRODUCT_INFO_LINK_SELECTOR = "div.productInfo > div > h3 > a";

    private static final String HREF = "href";
    private static final String POUND = "£";
    private static final String UNIT = "/unit";

    public List<Product> productFromScraping(DomNavigator domNavigator) {
        if (!domNavigator.select(PRODUCT_CONTAINER_SELECTOR).isEmpty()) {
            return domNavigator.select(PRODUCT_CONTAINER_SELECTOR)
                    .stream().map(this::extractProduct).filter(Optional::isPresent).map(Optional::get).collect(Collectors.toList());
        } else {
            return new ArrayList<>();
        }
    }

    private Optional<Product> extractProduct(DomElement domElement) {
        if (isValidProductSection(domElement)) {
            return Optional.of(Product.builder()
                    .title(domElement.select(PRODUCT_NAME_SELECTOR).stream().findFirst().map(DomElement::text).orElse(""))
                    .unitPrice(extractJustNumeralPrice(domElement.select(PRICE_PER_UNIT_SELECTOR).stream().findFirst().map(DomElement::text).orElse("")))
                    .productLink(domElement.select(PRODUCT_INFO_LINK_SELECTOR).stream().findFirst().map(element -> element.attr(HREF)).orElse(""))
                    .build());
        } else {
            return Optional.empty();
        }
    }

    private boolean isValidProductSection(DomElement domElement) {
        return (!(domElement.select(PRODUCT_NAME_SELECTOR).isEmpty() &&
                domElement.select(PRICE_PER_UNIT_SELECTOR).isEmpty() &&
                domElement.select(PRODUCT_INFO_LINK_SELECTOR).isEmpty()));
    }


    private String extractJustNumeralPrice(String priceWithLabels) {
        return Optional.ofNullable(priceWithLabels).map(
                s -> s.replace(POUND, "").replace(UNIT, "")).orElse(null);
    }
}
