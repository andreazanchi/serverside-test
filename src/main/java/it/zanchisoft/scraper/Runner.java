package it.zanchisoft.scraper;

import com.google.inject.Guice;
import com.google.inject.Inject;
import it.zanchisoft.scraper.config.ScraperRunnerModule;
import it.zanchisoft.scraper.response.ScrapeResult;
import lombok.AllArgsConstructor;

@AllArgsConstructor(onConstructor = @__({ @Inject }))
public class Runner {

    @Inject private ProductListScraper productListScraper;
    @Inject private ProductScraper productScraper;
    @Inject private HtmlFetcher htmlFetcher;
    @Inject private DomNavigatorFactory domNavigatorFactory;
    @Inject private Calculator calculator;
    @Inject private OutputFormatter outputFormatter;

    public static void main(String args[]) {
        Runner runner = Guice.createInjector(new ScraperRunnerModule()).getInstance(Runner.class);
        runner.run();
    }

    private void run() {
        Scraper scraper = new Scraper(productListScraper, productScraper, htmlFetcher, domNavigatorFactory, calculator);
        ScrapeResult scrapeResult = scraper.scrape("https://jsainsburyplc.github.io/serverside-test/site/www.sainsburys.co.uk/webapp/wcs/stores/servlet/gb/groceries/berries-cherries-currants6039.html");
        System.out.println(outputFormatter.output(scrapeResult));
    }


}
