package it.zanchisoft.scraper.navigator;

import it.zanchisoft.scraper.DomElement;
import it.zanchisoft.scraper.DomNavigator;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.util.List;
import java.util.stream.Collectors;

public class DomNavigatorJsoup implements DomNavigator {

    private Document dom;

    public DomNavigatorJsoup(String html) {
        this.dom = Jsoup.parse(html);
    }

    @Override
    public List<DomElement> select(String cssSelector) {
        return dom.select(cssSelector).stream().map(this::wrapElement).collect(Collectors.toList());
    }

    private DomElement wrapElement(Element element) {
        return new DomElementJsoup(element);
    }
}
