package it.zanchisoft.scraper.navigator;

import it.zanchisoft.scraper.DomNavigator;
import it.zanchisoft.scraper.DomNavigatorFactory;

public class DomNavigatorFactoryJsoup implements DomNavigatorFactory {
    @Override
    public DomNavigator buildFor(String body) {
        return new DomNavigatorJsoup(body);
    }
}
