package it.zanchisoft.scraper.navigator;

import it.zanchisoft.scraper.DomElement;
import lombok.AllArgsConstructor;
import org.jsoup.nodes.Element;

import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor
public class DomElementJsoup implements DomElement{

    private Element element;

    @Override
    public String text() {
        return element.text();
    }

    @Override
    public String attr(String attr) {
        return element.attr(attr);
    }

    @Override
    public List<DomElement> select(String s) {
        return element.select(s).stream().map(this::wrapElement).collect(Collectors.toList());
    }

    private DomElement wrapElement(Element element) {
        return new DomElementJsoup(element);
    }
}
