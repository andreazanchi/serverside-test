package it.zanchisoft.scraper;

import it.zanchisoft.scraper.exceptions.ProductPageNotReachableException;
import it.zanchisoft.scraper.response.ProductEntry;
import it.zanchisoft.scraper.response.ScrapeResult;
import it.zanchisoft.scraper.response.Total;
import lombok.AllArgsConstructor;
import lombok.extern.java.Log;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@AllArgsConstructor
@Log
public class Scraper {

    public static final RuntimeException NO_ENTRY_PAGE_EXCEPTION = new RuntimeException("Can't fetch the entry page");
    public static final String PAGE_DETAIL_NOT_REACHABLE = "Page detail not reachable for %s, it will be ignored";

    private final ProductListScraper productListScraper;
    private final ProductScraper productScraper;
    private final HtmlFetcher htmlFetcher;
    private final DomNavigatorFactory domNavigatorFactory;
    private final Calculator calculator;

    public ScrapeResult scrape(final String entryUrl) {
        List<Product> products = getProductsList(entryUrl);
        List<ProductEntry> productEntries = products.stream().map(product -> getProductEntry(entryUrl, product))
                .filter(Optional::isPresent).map(Optional::get).collect(Collectors.toList());
        Total total = buildTotalFrom(productEntries);
        return ScrapeResult.builder().results(productEntries).total(total).build();
    }

    private Optional<ProductEntry> getProductEntry(String entryUrl, Product product) {
        Optional<ProductEntry> ret = Optional.empty();
        try {
            ProductDetail productDetail = getProductDetail(product, entryUrl);
            ret = Optional.of(buildProductEntry(product, productDetail));
        } catch (ProductPageNotReachableException e) {
            log.warning(String.format(PAGE_DETAIL_NOT_REACHABLE, product.getTitle()));
        }
        return ret;
    }

    private List<Product> getProductsList(String entryUrl) {
        String pageBody = htmlFetcher.getBody(entryUrl).orElseThrow(() -> NO_ENTRY_PAGE_EXCEPTION);
        DomNavigator productListDomNav = domNavigatorFactory.buildFor(pageBody);
        return productListScraper.productFromScraping(productListDomNav);
    }

    private Total buildTotalFrom(List<ProductEntry> productEntries) {
        float totalGross = calculator.calculateTotalGrossFor(productEntries);
        float totalVat = calculator.calculateTotalVatFor(productEntries);
        return new Total(convertToString(totalGross), convertToString(totalVat));
    }

    private ProductDetail getProductDetail(Product product, String entryUrl) throws ProductPageNotReachableException {
        String pageBody = fetchProductDetailPage(product, entryUrl);
        DomNavigator productDomNavigator = domNavigatorFactory.buildFor(pageBody);
        return productScraper.productDetailFromScraping(productDomNavigator);
    }

    private String fetchProductDetailPage(Product product, String entryUrl) throws ProductPageNotReachableException {
        String fullProductLink = htmlFetcher.buildFullPath(entryUrl, product.getProductLink()).orElse("");
        return htmlFetcher.getBody(fullProductLink).orElseThrow(ProductPageNotReachableException::new);
    }

    private String convertToString(float floatValue) {
        return BigDecimal.valueOf(floatValue).setScale(2, RoundingMode.HALF_UP).toPlainString();
    }

    private ProductEntry buildProductEntry(Product product, ProductDetail productDetail) {
        return ProductEntry.builder()
                .title(product.getTitle())
                .description(productDetail.getDescription())
                .kCalPer100g(productDetail.getKCalPer100g())
                .unitPrice(Float.parseFloat(product.getUnitPrice()))
                .build();
    }
}
