package it.zanchisoft.scraper;

import it.zanchisoft.scraper.response.ProductEntry;

import java.util.List;

public class Calculator {

    private static final int VAT_PERCENTAGE = 20;

    public float calculateVat(float unitPrice) {
        return unitPrice - unitPrice * 100 / (100 + VAT_PERCENTAGE);
    }

//    public float calculateGross(float unitPrice) {
//        return unitPrice * 100 / (100 + VAT_PERCENTAGE);
//    }

    public float calculateTotalGrossFor(List<ProductEntry> productEntries) {
        return sumProductEntriesPrice(productEntries);
    }

    public float calculateTotalVatFor(List<ProductEntry> productEntries) {
        float sum = sumProductEntriesPrice(productEntries);
        return calculateVat(sum);
    }

    private float sumProductEntriesPrice(List<ProductEntry> productEntries) {
        return (float) productEntries.stream().mapToDouble(ProductEntry::getUnitPrice).sum();
    }
}
