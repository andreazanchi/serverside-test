package it.zanchisoft.scraper;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class Product {
    private String title;
    private String unitPrice;
    private String productLink;
}
