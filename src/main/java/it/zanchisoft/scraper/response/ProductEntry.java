package it.zanchisoft.scraper.response;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class ProductEntry {

    private String title;
    private String kCalPer100g;
    private float unitPrice;
    private String description;
}
