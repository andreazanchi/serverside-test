package it.zanchisoft.scraper.response;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class Total {
    private String gross;
    private String vat;
}
