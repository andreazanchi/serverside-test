package it.zanchisoft.scraper.response;

import lombok.Builder;
import lombok.Getter;

import java.util.List;

@Getter
@Builder
public class ScrapeResult {

    private final List<ProductEntry> results;
    private final Total total;
}
