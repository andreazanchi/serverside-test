package it.zanchisoft.scraper;

import java.util.List;

public interface DomElement {

    String text();
    String attr(String attr);
    List<DomElement> select(String s);
}
