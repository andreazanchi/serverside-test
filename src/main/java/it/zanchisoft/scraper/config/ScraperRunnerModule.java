package it.zanchisoft.scraper.config;

import com.google.inject.AbstractModule;
import it.zanchisoft.scraper.*;
import it.zanchisoft.scraper.http.HtmlFetcherImpl;
import it.zanchisoft.scraper.jsonoutput.OutputFormatterJson;
import it.zanchisoft.scraper.navigator.DomNavigatorFactoryJsoup;

public class ScraperRunnerModule extends AbstractModule{
    @Override
    protected void configure() {
        super.configure();
        bind(ProductScraper.class);
        bind(ProductListScraper.class);
        bind(HtmlFetcher.class).to(HtmlFetcherImpl.class);
        bind(Calculator.class);
        bind(DomNavigatorFactory.class).to(DomNavigatorFactoryJsoup.class);
        bind(OutputFormatter.class).to(OutputFormatterJson.class);
    }
}
