package it.zanchisoft.scraper;

import it.zanchisoft.scraper.response.ScrapeResult;

public interface OutputFormatter {
    String output(ScrapeResult scrapeResult);
}
