package it.zanchisoft.scraper;

import java.util.Optional;

public interface HtmlFetcher {

    Optional<String> getBody(String url);
    Optional<String> buildFullPath(String baseUrl, String relativeUrl);
}
