package it.zanchisoft.scraper.jsonoutput;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import it.zanchisoft.scraper.OutputFormatter;
import it.zanchisoft.scraper.response.ScrapeResult;

public class OutputFormatterJson implements OutputFormatter {
    @Override
    public String output(ScrapeResult scrapeResult) {
        Gson gson = new GsonBuilder().setPrettyPrinting().disableHtmlEscaping().create();
        return gson.toJson(scrapeResult);
    }
}
