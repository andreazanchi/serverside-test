package it.zanchisoft.scraper;

import com.github.tomakehurst.wiremock.junit.WireMockRule;
import it.zanchisoft.scraper.http.HtmlFetcherImpl;
import it.zanchisoft.scraper.navigator.DomNavigatorFactoryJsoup;
import it.zanchisoft.scraper.response.ScrapeResult;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;

public class ScraperIntegrationTest {

    public static final String ENTRY_PAGE_WIREMOCK = "http://localhost:8080/serverside-test/site/www.sainsburys.co.uk/webapp/wcs/stores/servlet/gb/groceries/berries-cherries-currants6039.html";
    private Scraper scraper = new Scraper(new ProductListScraper(), new ProductScraper(), new HtmlFetcherImpl(), new DomNavigatorFactoryJsoup(), new Calculator());

    @Rule
    public WireMockRule wireMockRule = new WireMockRule();

    @Test
    public void test_scraper_output_contains_something() {
        ScrapeResult scrapeResult = scraper.scrape(ENTRY_PAGE_WIREMOCK);
        Assert.assertNotNull(scrapeResult);
    }

    @Test
    public void test_scraper_output_contains_seventeen_products() {
        ScrapeResult scrapeResult = scraper.scrape(ENTRY_PAGE_WIREMOCK);
        Assert.assertEquals(17, scrapeResult.getResults().size());
    }

    @Test
    public void test_scraper_output_total_gross_is_correct() {
        ScrapeResult scrapeResult = scraper.scrape(ENTRY_PAGE_WIREMOCK);
        Assert.assertEquals("39.50", scrapeResult.getTotal().getGross());
    }

    @Test
    public void test_scraper_output_total_vat_is_correct() {
        ScrapeResult scrapeResult = scraper.scrape(ENTRY_PAGE_WIREMOCK);
        Assert.assertEquals("6.58", scrapeResult.getTotal().getVat());
    }




}
