package it.zanchisoft.scraper;

import org.apache.commons.io.IOUtils;

import java.io.IOException;

public class ProductScraperHtml {

    String getProductPage() {
        String fileContent = null;
        try {
            fileContent =  IOUtils.toString(
                    this.getClass().getResourceAsStream("productPage.html"),
                    "UTF-8"
            );
        } catch (IOException e) {
            fileContent = "";
            e.printStackTrace();
        }
        return fileContent;
    }
}
