package it.zanchisoft.scraper;

import org.apache.commons.io.IOUtils;

import java.io.IOException;

public class ScraperTestHtml {

     String getListPageOneElement() {
        String fileContent = null;
        try {
            fileContent =  IOUtils.toString(
                    this.getClass().getResourceAsStream("listPageOneElement.html"),
                    "UTF-8"
            );
        } catch (IOException e) {
            fileContent = "";
            e.printStackTrace();
        }
        return fileContent;
    }

    String getListPageTwoeElements() {
        String fileContent = null;
        try {
            fileContent =  IOUtils.toString(
                    this.getClass().getResourceAsStream("listPageTwoElements.html"),
                    "UTF-8"
            );
        } catch (IOException e) {
            fileContent = "";
            e.printStackTrace();
        }
        return fileContent;
    }
}
