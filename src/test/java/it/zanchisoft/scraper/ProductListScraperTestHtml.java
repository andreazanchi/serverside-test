package it.zanchisoft.scraper;

import org.apache.commons.io.IOUtils;

import java.io.IOException;

public class ProductListScraperTestHtml {

    String getElementStrawberries() {
        String fileContent = null;
        try {
            fileContent =  IOUtils.toString(
                    this.getClass().getResourceAsStream("elementStrawberries.html"),
                    "UTF-8"
            );
        } catch (IOException e) {
            fileContent = "";
            e.printStackTrace();
        }
        return fileContent;
    }

    String getElementBlueberries() {
        String fileContent = null;
        try {
            fileContent =  IOUtils.toString(
                    this.getClass().getResourceAsStream("elementBlueberries.html"),
                    "UTF-8"
            );
        } catch (IOException e) {
            fileContent = "";
            e.printStackTrace();
        }
        return fileContent;
    }

    String getElementStrawberriesWithCrossSell() {
        String fileContent = null;
        try {
            fileContent =  IOUtils.toString(
                    this.getClass().getResourceAsStream("elementStrawberriesWithCrossSell.html"),
                    "UTF-8"
            );
        } catch (IOException e) {
            fileContent = "";
            e.printStackTrace();
        }
        return fileContent;
    }




}
