package it.zanchisoft.scraper;

import it.zanchisoft.scraper.navigator.DomNavigatorJsoup;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.List;

public class ProductListScraperTest {



    private static ProductListScraper scraper;
    private static DomNavigator singleBerryDomNavigator;
    private static DomNavigator twoBerriesDomNavigator;
    private static ProductListScraperTestHtml productListScraperTestHtml = new ProductListScraperTestHtml();

    @BeforeClass
    public static void setup() {
        scraper = new ProductListScraper();
        singleBerryDomNavigator = new DomNavigatorJsoup(productListScraperTestHtml.getElementStrawberries());
        twoBerriesDomNavigator = new DomNavigatorJsoup(productListScraperTestHtml.getElementStrawberries()+ productListScraperTestHtml.getElementBlueberries());
    }

    @Test
    public void when_html_contains_one_strawberries_item_then_scraper_returns_not_empty_list() {
        List<Product> result = scraper.productFromScraping(singleBerryDomNavigator);
        Assert.assertFalse(result == null || result.isEmpty());
    }

    @Test
    public void when_html_contains_one_strawberries_item_then_element_has_title() {
        Product element = scraper.productFromScraping(singleBerryDomNavigator).get(0);
        Assert.assertEquals("Sainsbury's Strawberries 400g", element.getTitle());
    }

    @Test
    public void when_html_contains_one_strawberries_item_then_element_has_unit_price() {
        Product element = scraper.productFromScraping(singleBerryDomNavigator).get(0);
        Assert.assertEquals("1.75", element.getUnitPrice());
    }

    @Test
    public void when_html_contains_one_strawberries_item_then_element_has_product_page_link() {
        Product element = scraper.productFromScraping(singleBerryDomNavigator).get(0);
        Assert.assertEquals("../../../../../../shop/gb/groceries/berries-cherries-currants/sainsburys-british-strawberries-400g.html", element.getProductLink());
    }

    @Test
    public void when_html_contains_two_berries_item_then_scraper_returns_two_product() {
        List<Product> result = scraper.productFromScraping(twoBerriesDomNavigator);
        Assert.assertEquals(2, result.size());
    }

    @Test
    public void when_html_contains_two_berries_items_then_second_element_has_title() {
        Product element = scraper.productFromScraping(twoBerriesDomNavigator).get(1);
        Assert.assertEquals("Sainsbury's Blueberries 200g", element.getTitle());
    }

    @Test
    public void when_html_contains_two_berries_items_then_both_elements_have_title() {
        Product element = scraper.productFromScraping(twoBerriesDomNavigator).get(0);
        Assert.assertEquals("Sainsbury's Strawberries 400g", element.getTitle());
        Product element2 = scraper.productFromScraping(twoBerriesDomNavigator).get(1);
        Assert.assertEquals("Sainsbury's Blueberries 200g", element2.getTitle());
    }

    @Test
    public void when_html_strawberries_and_cross_sell_ignore_cross_sell_item() {
        DomNavigator domNavigator = new DomNavigatorJsoup(productListScraperTestHtml.getElementStrawberriesWithCrossSell());
        List<Product> elements = scraper.productFromScraping(domNavigator);
        Assert.assertEquals(1, elements.size());
        Assert.assertEquals("Sainsbury's Strawberries, Taste the Difference 300g", elements.get(0).getTitle());
    }


}
