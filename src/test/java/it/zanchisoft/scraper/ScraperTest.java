package it.zanchisoft.scraper;

import it.zanchisoft.scraper.navigator.DomNavigatorFactoryJsoup;
import it.zanchisoft.scraper.response.ScrapeResult;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.Optional;

public class ScraperTest {

    private static final ProductScraperHtml PRODUCT_SCRAPER_HTML = new ProductScraperHtml();
    private static final ScraperTestHtml SCRAPER_TEST_HTML = new ScraperTestHtml();
    private static Scraper scraper;

    @BeforeClass
    public static void setup() {
        HtmlFetcher htmlFetcherMock = Mockito.mock(HtmlFetcher.class);
        Mockito.when(htmlFetcherMock.getBody("mockedListPageWithOneElement")).thenReturn(Optional.of(SCRAPER_TEST_HTML.getListPageOneElement()));
        Mockito.when(htmlFetcherMock.getBody("mockedListPageWithTwoElements")).thenReturn(Optional.of(SCRAPER_TEST_HTML.getListPageTwoeElements()));
        Mockito.when(htmlFetcherMock.getBody("mockedProductDetailPageUrl")).thenReturn(Optional.of(PRODUCT_SCRAPER_HTML.getProductPage()));
        Mockito.when(htmlFetcherMock.buildFullPath("mockedListPageWithOneElement", "mockedProductDetailPageUrl")).thenReturn(Optional.of("mockedProductDetailPageUrl"));
        Mockito.when(htmlFetcherMock.buildFullPath("mockedListPageWithTwoElements", "mockedProductDetailPageUrl")).thenReturn(Optional.of("mockedProductDetailPageUrl"));
        scraper = new Scraper(new ProductListScraper(), new ProductScraper(), htmlFetcherMock, new DomNavigatorFactoryJsoup(), new Calculator());
    }

    @Test
    public void test_when_list_has_one_product_on_page_return_one_result() {
        ScrapeResult result = scraper.scrape("mockedListPageWithOneElement");
        Assert.assertEquals(1, result.getResults().size());
    }

    @Test
    public void test_when_list_has_one_product_on_page_all_product_values_are_correct() {
        ScrapeResult result = scraper.scrape("mockedListPageWithOneElement");
        Assert.assertEquals("Sainsbury's Strawberries 400g", result.getResults().get(0).getTitle());
        Assert.assertEquals(1.75, result.getResults().get(0).getUnitPrice(), 0);
        Assert.assertEquals("33", result.getResults().get(0).getKCalPer100g());
        Assert.assertEquals("by Sainsbury's strawberries", result.getResults().get(0).getDescription());
    }

    @Test
    public void test_when_list_has_one_product_on_page_verify_vat_is_correct() {
        ScrapeResult result = scraper.scrape("mockedListPageWithOneElement");
        Assert.assertEquals("0.29", result.getTotal().getVat());
    }

    @Test
    public void test_when_list_has_one_product_on_page_verify_gross_is_correct() {
        ScrapeResult result = scraper.scrape("mockedListPageWithOneElement");
        Assert.assertEquals("1.75", result.getTotal().getGross());
    }

    @Test
    public void test_when_list_has_two_products_on_page_we_get_two_products_with_correct_values() {
        ScrapeResult result = scraper.scrape("mockedListPageWithTwoElements");
        Assert.assertEquals("Sainsbury's Strawberries 400g", result.getResults().get(0).getTitle());
        Assert.assertEquals(1.75, result.getResults().get(0).getUnitPrice(), 0);
        Assert.assertEquals("33", result.getResults().get(0).getKCalPer100g());
        Assert.assertEquals("by Sainsbury's strawberries", result.getResults().get(0).getDescription());
        Assert.assertEquals("Sainsbury's Strawberries 400g", result.getResults().get(1).getTitle());
        Assert.assertEquals(1.75, result.getResults().get(1).getUnitPrice(), 0);
        Assert.assertEquals("33", result.getResults().get(1).getKCalPer100g());
        Assert.assertEquals("by Sainsbury's strawberries", result.getResults().get(1).getDescription());
    }

    @Test
    public void test_when_list_has_two_products_on_page_verify_gross_is_correct() {
        ScrapeResult result = scraper.scrape("mockedListPageWithTwoElements");
        Assert.assertEquals("3.50", result.getTotal().getGross());
    }

    @Test
    public void test_when_list_has_two_products_on_page_verify_vat_is_correct() {
        ScrapeResult result = scraper.scrape("mockedListPageWithTwoElements");
        Assert.assertEquals("0.58", result.getTotal().getVat());
    }
}
