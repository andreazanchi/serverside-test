package it.zanchisoft.scraper;

import it.zanchisoft.scraper.navigator.DomNavigatorJsoup;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class ProductScraperTest {

    private ProductScraper productScraper;
    private DomNavigator domNavigator;

    @Before
    public void setup() {
        this.domNavigator = new DomNavigatorJsoup(new ProductScraperHtml().getProductPage());
        this.productScraper = new ProductScraper();
    }

    @Test
    public void test_when_product_has_calories_return_calories() {
        ProductDetail productDetail = productScraper.productDetailFromScraping(domNavigator) ;
        Assert.assertEquals("33", productDetail.getKCalPer100g());
    }

    @Test
    public void test_when_product_has_title_return_title() {
        ProductDetail productDetail = productScraper.productDetailFromScraping(domNavigator) ;
        Assert.assertEquals("by Sainsbury's strawberries", productDetail.getDescription());
    }
}
