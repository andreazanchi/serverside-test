package it.zanchisoft.scraper;

import com.github.tomakehurst.wiremock.junit.WireMockRule;
import it.zanchisoft.scraper.http.HtmlFetcherImpl;
import it.zanchisoft.scraper.jsonoutput.OutputFormatterJson;
import it.zanchisoft.scraper.navigator.DomNavigatorFactoryJsoup;
import it.zanchisoft.scraper.response.ScrapeResult;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;

public class OutputFormatterIntegrationTest {


    public static final String ENTRY_PAGE_WIREMOCK = "http://localhost:8080/serverside-test/site/www.sainsburys.co.uk/webapp/wcs/stores/servlet/gb/groceries/berries-cherries-currants6039.html";
    private Scraper scraper = new Scraper(new ProductListScraper(), new ProductScraper(), new HtmlFetcherImpl(), new DomNavigatorFactoryJsoup(), new Calculator());
    private OutputFormatter outputFormatter = new OutputFormatterJson();

    @Rule
    public WireMockRule wireMockRule = new WireMockRule();

    @Test
    public void test_json_output_is_correct() {
        ScrapeResult scrapeResult = scraper.scrape(ENTRY_PAGE_WIREMOCK);
        String output = outputFormatter.output(scrapeResult);
        String expected = "{\n" +
                "  \"results\": [\n" +
                "    {\n" +
                "      \"title\": \"Sainsbury's Strawberries 400g\",\n" +
                "      \"kCalPer100g\": \"33\",\n" +
                "      \"unitPrice\": 1.75,\n" +
                "      \"description\": \"by Sainsbury's strawberries\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"title\": \"Sainsbury's Blueberries 200g\",\n" +
                "      \"unitPrice\": 1.75,\n" +
                "      \"description\": \"by Sainsbury's blueberries\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"title\": \"Sainsbury's Raspberries 225g\",\n" +
                "      \"unitPrice\": 1.75,\n" +
                "      \"description\": \"by Sainsbury's raspberries\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"title\": \"Sainsbury's Blackberries, Sweet 150g\",\n" +
                "      \"kCalPer100g\": \"32\",\n" +
                "      \"unitPrice\": 1.5,\n" +
                "      \"description\": \"by Sainsbury's blackberries\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"title\": \"Sainsbury's Blueberries 400g\",\n" +
                "      \"kCalPer100g\": \"45\",\n" +
                "      \"unitPrice\": 3.25,\n" +
                "      \"description\": \"by Sainsbury's blueberries\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"title\": \"Sainsbury's Blueberries, SO Organic 150g\",\n" +
                "      \"unitPrice\": 2.0,\n" +
                "      \"description\": \"So Organic blueberries\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"title\": \"Sainsbury's Raspberries, Taste the Difference 150g\",\n" +
                "      \"unitPrice\": 2.5,\n" +
                "      \"description\": \"Ttd raspberries\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"title\": \"Sainsbury's Cherries 400g\",\n" +
                "      \"unitPrice\": 2.5,\n" +
                "      \"description\": \"by Sainsbury's Family Cherry Punnet\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"title\": \"Sainsbury's Blackberries, Tangy 150g\",\n" +
                "      \"unitPrice\": 1.5,\n" +
                "      \"description\": \"by Sainsbury's blackberries\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"title\": \"Sainsbury's Strawberries, Taste the Difference 300g\",\n" +
                "      \"kCalPer100g\": \"27\",\n" +
                "      \"unitPrice\": 2.5,\n" +
                "      \"description\": \"Ttd strawberries\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"title\": \"Sainsbury's Cherry Punnet 200g\",\n" +
                "      \"unitPrice\": 1.5,\n" +
                "      \"description\": \"Cherries\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"title\": \"Sainsbury's Mixed Berries 300g\",\n" +
                "      \"unitPrice\": 3.5,\n" +
                "      \"description\": \"by Sainsbury's mixed berries\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"title\": \"Sainsbury's Mixed Berry Twin Pack 200g\",\n" +
                "      \"unitPrice\": 2.75,\n" +
                "      \"description\": \"Mixed Berries\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"title\": \"Sainsbury's Redcurrants 150g\",\n" +
                "      \"unitPrice\": 2.5,\n" +
                "      \"description\": \"by Sainsbury's redcurrants\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"title\": \"Sainsbury's Cherry Punnet, Taste the Difference 200g\",\n" +
                "      \"unitPrice\": 2.5,\n" +
                "      \"description\": \"Cherry Punnet\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"title\": \"Sainsbury's Blackcurrants 150g\",\n" +
                "      \"unitPrice\": 1.75,\n" +
                "      \"description\": \"\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"title\": \"Sainsbury's British Cherry & Strawberry Pack 600g\",\n" +
                "      \"unitPrice\": 4.0,\n" +
                "      \"description\": \"British Cherry & Strawberry Mixed Pack\"\n" +
                "    }\n" +
                "  ],\n" +
                "  \"total\": {\n" +
                "    \"gross\": \"39.50\",\n" +
                "    \"vat\": \"6.58\"\n" +
                "  }\n" +
                "}";
        Assert.assertEquals(expected, output);
    }
}
