package it.zanchisoft.scraper;

import it.zanchisoft.scraper.response.ProductEntry;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;

public class CalculatorTest {

    private Calculator calculator;

    @Before
    public void setup() {
        this.calculator = new Calculator();
    }

//    @Test
//    public void test_when_given_total_calculate_gross() {
//        float result = calculator.calculateGross( 1.75f);
//        Assert.assertEquals(1.46, result, 0.01);
//    }

    @Test
    public void test_when_given_total_calculate_vat() {
        float result = calculator.calculateVat( 5.00f);
        Assert.assertEquals(0.83, result, 0.01);
    }

//    @Test
//    public void test_when_given_one_product_entry_calculate_total_gross() {
//        float result = calculator.calculateTotalGrossFor(Arrays.asList(ProductEntry.builder().unitPrice(1.75f).build()));
//        Assert.assertEquals(1.46f, result, 0.01);
//    }

    @Test
    public void test_when_given_one_product_entry_calculate_total_vat() {
        float result = calculator.calculateTotalVatFor(Arrays.asList(ProductEntry.builder().unitPrice(1.75f).build()));
        Assert.assertEquals(0.29f, result, 0.01);
    }
//
//    @Test
//    public void test_when_given_two_products_entries_calculate_total_gross() {
//        float result = calculator.calculateTotalGrossFor(Arrays.asList(
//                ProductEntry.builder().unitPrice(1.75f).build(), ProductEntry.builder().unitPrice(1.75f).build(),
//                ProductEntry.builder().unitPrice(1.75f).build()));
//        Assert.assertEquals(4.38f, result, 0.01);
//    }

    @Test
    public void test_when_given_two_products_entries_calculate_total_vat() {
        float result = calculator.calculateTotalVatFor(Arrays.asList(
                ProductEntry.builder().unitPrice(1.75f).build(), ProductEntry.builder().unitPrice(1.75f).build(),
                ProductEntry.builder().unitPrice(1.5f).build()));
        Assert.assertEquals(0.83f, result, 0.01);
    }

}
