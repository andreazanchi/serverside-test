package it.zanchisoft.scraper;

import it.zanchisoft.scraper.http.HtmlFetcherImpl;
import org.junit.Assert;
import org.junit.Test;

public class HtmlFetcherTest {

    private final HtmlFetcher htmlFetcher = new HtmlFetcherImpl();

    @Test
    public void test_when_given_base_path_and_relative_path_build_full_path() {
        String fullPath = htmlFetcher.buildFullPath("https://jsainsburyplc.github.io/serverside-test/site/www.sainsburys.co.uk/webapp/wcs/stores/servlet/gb/groceries/berries-cherries-currants6039.html"
        ,"../../../../../../shop/gb/groceries/berries-cherries-currants/sainsburys-british-strawberries-400g.html").orElse("");
        Assert.assertEquals("https://jsainsburyplc.github.io/serverside-test/site/www.sainsburys.co.uk/shop/gb/groceries/berries-cherries-currants/sainsburys-british-strawberries-400g.html", fullPath);
    }


}
