package it.zanchisoft.scraper;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.junit.Assert;
import org.junit.Test;

public class JsoupPlayground {

    @Test
    public void test_load_document_from_html_string() {
        Document document = Jsoup.parse("<html><body><div class=\"productNameAndPromotions\">\n" +
                "                        \n" +
                "                                  <h3>\n" +
                "                                    <a href=\"../../../../../../shop/gb/groceries/berries-cherries-currants/sainsburys-british-strawberries-400g.html\" >\n" +
                "                                        Sainsbury's Strawberries 400g\n" +
                "                                        <img src=\"../../../../../../../c2.sainsburys.co.uk/wcsstore7.23.1.52/SainsburysStorefrontAssetStore/wcassets/product_images/2017/May/media_7555699_L.jpg\" alt=\"\" />\n" +
                "                                    </a>\n" +
                "                                </h3>\n" +
                "                              \n" +
                "                    </div></body></html>\n");
        Assert.assertEquals("Sainsbury's Strawberries 400g", document.select(".productNameAndPromotions a").text());
    }

}
